package com.example.lib;

public class ClassComputing implements CalculatorInterface {
    private double n1;
    private double n2;
    private String op;
    private double Total;

    public void setNum1(double a) {


            if (a >= 0 && a <= 1000)
                n1 = a;
             else
             n1=1000;

    }

    public double getN1() {
        return n1;
    }



    public void setNum2(double a) {


            if (a >= 0 && a <= 1000)
                n2 = a;
            else
                n1=1000;
    }

    public double getN2() {
        return n2;
    }



    public void setOp(String a) {

        try {
            if (a.equals("+") || a.equals("-") || a.equals("/") || a.equals("*")  )
                op = a;
        } catch (Exception e) {
            System.out.println("Wrong Enter Operator");
        }
    }

    public String getOp() {
        return op;
    }


    @Override
    public void Num1() {
        System.out.println("Enter the First Number:");
    }

    @Override
    public void Num2() {
        System.out.println("Enter the Second Number:");
    }

    @Override
    public void OP() {
        System.out.println("Enter Operator:");
    }



    @Override
    public double CalcRequest() {
        switch (op)
        {
            case "+":
                Total=getN1()+getN2();
                break;
            case "*":
                Total=getN1()*getN2();
                break;
            case "/":
                Total=getN1()/getN2();
                break;
            case "-":
                Total=getN1()-getN2();
                break;
    }
    return Total;
}

    @Override
    public void Result() {
        System.out.format("%f  %s  %f = %f",n1,op,n2,Total);
    }
}
